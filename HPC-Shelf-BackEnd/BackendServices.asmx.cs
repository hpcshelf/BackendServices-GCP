﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.Services;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Compute.v1;
using Google.Apis.Services;
using System.Threading.Tasks;
using Google.Apis.Compute.v1.Data;
using System.IO;
using System.Collections.Concurrent;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

namespace org.hpcshelf.backend
{
    public interface IBackendServices
    {
        string deploy(string platform_config);
        string release(string platform_address);
    }


    public class BackendServices : WebService, IBackendServices
    {
        private static string ScriptRunException = "Script RunException!!!";

        private static readonly string default_machine_type = "n1-standard-1";  
        private static readonly string default_zone = "us-central1-a";
        private static readonly string default_network = "default";
        // private static readonly string default_image_id = "hpcshelf-virtual-platform-image-new";
        // private static string default_project { get { return File.ReadAllLines(Utils.HOME + "PROJECT_ID")[0]; } }


        private string GCP_VIRTUALPLATFORM_IMAGE_ID { get { return File.ReadAllLines("GCP_VIRTUALPLATFORM_IMAGE_ID")[0]; } }

        public BackendServices() { }

        public static GoogleCredential GetCredential()
        {
            GoogleCredential credential = Task.Run(() => GoogleCredential.GetApplicationDefaultAsync()).Result;
            if (credential.IsCreateScopedRequired)
            {
                credential = credential.CreateScoped("https://www.googleapis.com/auth/cloud-platform");
            }
            return credential;
        }

        private static int deploy_id_counter = 0;
        private static ConcurrentQueue<int> deploy_id_queue = new ConcurrentQueue<int>();
        private string HPCShelf_HOME = Environment.GetEnvironmentVariable("HPCShelf_HOME");
        private string MONO_HOME = Environment.GetEnvironmentVariable("MONO_HOME");

        //private static IDictionary<string, int> deploy_id_map = new Dictionary<string, int>();
        private static readonly object lock_deploy_id = new object();


        [WebMethod]
        public string deploy(string platform_config)
        {
            int deploy_id = default(int);
            try
            {
                if (!deploy_id_queue.TryDequeue(out deploy_id))
                    lock (lock_deploy_id)
                    {
                        deploy_id = deploy_id_counter++;
                    }

                Console.WriteLine("deploy id ... {0} ", deploy_id);

                Console.WriteLine("STARTING DEPLOYING -- {0}", platform_config == null);
                Console.WriteLine("PLATFORM: {0}", platform_config);

                string[] config = platform_config.Split('\n', '\t', '\r', ';', ' ', ',');

                #region IaaS Platform Dependent


                IList<Instance> VirtualMachines = new List<Instance>();
                ComputeService computeService;
                string project;
                string zone;
                createVirtualMachines(config, VirtualMachines, out project, out zone, out computeService);

                string platform_address_private = VirtualMachines[0].NetworkInterfaces[0].NetworkIP; // VirtualMachines.ElementAt(0).PublicIpAddress;
                string platform_address_public = VirtualMachines[0].NetworkInterfaces[0].AccessConfigs[0].NatIP; // .PrivateIpAddress;

                #endregion IaaS Platform Dependent

                generatePeerHosts(deploy_id, VirtualMachines);
                configure_cluster(deploy_id, platform_address_public);
                startShelf(deploy_id, platform_address_public, platform_address_private);

                if (VirtualMachines.Count > 0)
                {
                    int port_platform = 8080;
                    string platform_id = string.Format("http://{0}:{1}/PlatformServices.asmx", platform_address_public, port_platform);
                    running_instances[platform_id] = new Tuple<string, string, IList<Instance>, ComputeService>(project, zone, VirtualMachines, computeService);
                    return platform_id;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Invalid Config: " + e.Message);
                throw;
            }
            finally
            {
                deploy_id_queue.Enqueue(deploy_id);
            }

            return "";
        }

        private static IDictionary<string, Tuple<string, string, IList<Instance>, ComputeService>> running_instances = new ConcurrentDictionary<string, Tuple<string, string, IList<Instance>, ComputeService>>();

        private class ServiceAccountJson
        {
            public string type { get; set; }
            public string project_id { get; set; }
            public string private_key_id { get; set; }
            public string private_key { get; set; }
            public string client_email { get; set; }
            public string client_id { get; set; }
            public string auth_uri { get; set; }
            public string token_uri { get; set; }
            public string auth_provider_x509_cert_url { get; set; }
            public string client_x509_cert_url { get; set; }
        }

        private string project_id
        {
            get
            {
                string credentialsString = File.ReadAllText(Environment.GetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS"));
                ServiceAccountJson credential_data = JsonConvert.DeserializeObject<ServiceAccountJson>(credentialsString);
                return credential_data.project_id;
            }
        }

        [WebMethod]
        public string release(string platform_address)
        {
            if (!running_instances.ContainsKey(platform_address))
                throw new Exception(string.Format("There is no running platform named {0} to be released", platform_address));

            string project = running_instances[platform_address].Item1;
            string zone = running_instances[platform_address].Item2;
            ComputeService computeService = running_instances[platform_address].Item4;

            foreach (Instance instance in running_instances[platform_address].Item3)
            {
                string instance_name = null;
                // REQUEST INSTANCE CREATION
                InstancesResource.DeleteRequest request = computeService.Instances.Delete(project, zone, instance_name);
                request.Execute();
            }           

            running_instances.Remove(platform_address);

            //deploy_id_queue.Enqueue(deploy_id_map[platform_address]);

            string return_message = string.Format("{0}  released !", platform_address);
            return return_message;
        }

        private void createVirtualMachines(string[] config, 
                                           IList<Instance> virtual_machines, 
                                           out string project0, 
                                           out string region0, 
                                           out ComputeService computeService0)
        {



            GoogleCredential credential = GetCredential();

            computeService0 = new ComputeService(new BaseClientService.Initializer
            {
                HttpClientInitializer = credential,
                ApplicationName = "HPC Shelf - Google Backend",
            });


            // FETCH THE INSTANCE CONFIGURATION
            project0 = config[0].Equals("*") ? project_id : config[0];
            region0 = config[1].Equals("") ? default_zone : config[1];
            string image_id = config[2].Equals("*") ? GCP_VIRTUALPLATFORM_IMAGE_ID : config[2];
            string machine_type = config[3].Equals("*") ? default_machine_type : config[3];  //  e.g. "zones/us-east1-c/machineTypes/n1-standard-1";
            string network_id = config[4].Equals("*") ? default_network : config[4];
            int n = int.Parse(config[5]) + 1; // including root ...

            Console.WriteLine("Project: {0}", project0);
            Console.WriteLine("Region: {0}", region0);

            RegionsResource.GetRequest request_region = computeService0.Regions.Get(project0, region0);
            Region response = request_region.Execute();
            string zone0 = response.Zones[0];
            string[] z = zone0.Split('/');
            zone0 = z[z.Length - 1];

            Console.WriteLine("Zone: {0}", zone0);
            machine_type = machine_type.Replace(region0, zone0);

            Console.WriteLine("Machine Type: {0}", machine_type);

            // FETCH THE IMAGE
            ImagesResource.GetRequest request_image = computeService0.Images.Get(project0, image_id);
            Image image = request_image.Execute();

            // FETCH THE NETWORK
            NetworksResource.GetRequest get_network = computeService0.Networks.Get(project0, network_id);
            Network network = get_network.Execute();

            // CONFIGURE THE NetworkInterface OBJECT
            NetworkInterface network_interface = new NetworkInterface();
            network_interface.Network = network.SelfLink;

            // CONFIGURE THE NetworkInterface OBJECT
            NetworkInterface network_interface_local = new NetworkInterface();
            network_interface_local.Network = network.SelfLink;

            // CREATE AN AccessConfig OBJECT FOR HAVING A PUBLIC IP.
            AccessConfig access_config = new AccessConfig();
            network_interface.AccessConfigs = new List<AccessConfig>();
            network_interface.AccessConfigs.Add(access_config);

            Thread[] create_instance_threads = new Thread[n];
            Instance[] vms = new Instance[n];

            for (int j = 0; j < n; j++)
            {
                create_instance_threads[j] = new Thread((o) =>
                {
                    int jj = ((Tuple<int, ComputeService, string, string>)o).Item1;
                    ComputeService computeService = ((Tuple<int, ComputeService, string, string>)o).Item2;
                    string project = ((Tuple<int, ComputeService, string, string>)o).Item3;
                    string zone = ((Tuple<int, ComputeService, string, string>)o).Item4;

                    string timestamp = DateTime.Now.Millisecond.ToString();
                    string suffix = jj + timestamp;

                    // CREATE DISK FROM IMAGE FOR THE INSTANCE
                    Disk disk_description = new Disk();
                    disk_description.SourceImage = image.SelfLink;
                    disk_description.Name = "hpcshelf-disk-" + suffix;
                    DisksResource.InsertRequest request_disk = computeService.Disks.Insert(disk_description, project, zone);
                    request_disk.Execute();

                    // Thread.Sleep(2000);

                    // FETCH THE CREATED DISK
                    DisksResource.GetRequest get_disk = computeService.Disks.Get(project, zone, "hpcshelf-disk-" + suffix);
                    Disk new_disk; // = get_disk.Execute();

                    do
                    {
                        new_disk = get_disk.Execute();
                        Console.WriteLine("DISK {0} is {1}", new_disk.Name, new_disk.Status);
                        Thread.Sleep(500);
                    }
                    while (!new_disk.Status.Equals("READY"));

                    // CONFIGURE THE DISK TO BE ATTACHED IN THE INSTANCE
                    AttachedDisk disk = new AttachedDisk();
                    disk.Source = new_disk.SelfLink;
                    disk.Boot = true;


                    // CONFIGURE REQUEST INSTANCE OBJECT
                    Instance request_instance = new Instance();
                    request_instance.Name = "hpcshelf-virtualplatform-" + suffix;
                    request_instance.Zone = zone;
                    request_instance.MachineType = machine_type;
                    request_instance.Disks = new List<AttachedDisk>(new AttachedDisk[1] { disk });
                    request_instance.NetworkInterfaces = new List<NetworkInterface>(jj == 0 ? new NetworkInterface[1] { network_interface } : new NetworkInterface[1] { network_interface_local });
                    //Metadata.ItemsData m = new Metadata.ItemsData();
                    //m.Key = "ssh-keys";
                    //m.Value = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCcfPslLEw4/Lkw6SzHs6OVeyhQlUodObg16oJEg7t83e98YT9sc8Jbz/Pbwe9LoRs9k4E7B5tRO8P/HEgqc+AjGM5pV/Fb9xbrMBPgipXE5QcePIwmOCQvJMhJZXxSgjLTGjL/LrOK6QCLHHs5f9W+NjzA/58sK/O1ySgoXuGpjMXcMpZMUe6HSiGwGBJFQWhBFzHvNR3sCrWtUiqdmQ656l4jo0lMhLNHi9FPFzeKQppTuQJbrjBX24zNVr+69JmFiweSs/U2zD+lcYbtwjEjccZAoOGzn60k43Jlz1Cb0K6yuJIOVPeLHN3CTQaSHpD1xh811n4+6VyTD2mrAgBB ubuntu";
                    //request_instance.Metadata = new Metadata();
                    //request_instance.Metadata.Items = new List<Metadata.ItemsData>();
                    //request_instance.Metadata.Items.Add(m);

                    // REQUEST INSTANCE CREATION
                    InstancesResource.InsertRequest request = computeService.Instances.Insert(request_instance, project, zone);
                    request.Execute();
                    // Thread.Sleep(5000);

                    // FETCH THE INSTANCE
                    InstancesResource.GetRequest get_instance = computeService.Instances.Get(project, zone, request_instance.Name);
                    Instance instance; // = get_instance.Execute();

                    do
                    {
                        instance = get_instance.Execute();
                        Console.WriteLine("INSTANCE {0} is {1}", instance.Name, instance.Status);
                        Thread.Sleep(1000);
                    }
                    while (!instance.Status.Equals("RUNNING"));

                    vms[jj] = instance;
                });

            }

            for (int j = 0; j < n; j++)
                create_instance_threads[j].Start(new Tuple<int, ComputeService, string, string>(j, computeService0, project0, zone0));

            for (int j = 0; j < n; j++)
            {
                create_instance_threads[j].Join();
                virtual_machines.Add(vms[j]);
            }
        }


        public void configure_cluster(int deploy_id, string public_address)
        {
            try
            {
                Utils.commandExecBash(Path.Combine(Utils.SCRIPTS, "configure_cluster") + " " + public_address + " " + deploy_id);
            }
            catch (Exception e)
            {
                Console.WriteLine(ScriptRunException);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        public string startShelf(int deploy_id, string platform_address_global, string platform_address_local)
        {
            Console.WriteLine("startShelf enter: {0}", deploy_id);

            try
            {
                Thread thread = new Thread(() => xsp4_shelf_root_run(platform_address_global, platform_address_local));
                thread.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(ScriptRunException);
            }

            Console.WriteLine("startShelf exit: {0}", deploy_id);
            return platform_address_global;
        }

        private static void xsp4_shelf_root_run(string platform_address_global, string platform_address_local)
        {
            Utils.commandExecBash(Path.Combine(Utils.SCRIPTS, "startShelf") + " " + platform_address_global + " " + platform_address_local + " &");
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        protected void generatePeerHosts(int deploy_id, IList<Instance> VirtualMachines)
        {
            if (VirtualMachines.Count > 0)
            {
                Console.WriteLine("generatePeerHosts enter: {0}", deploy_id);

                IEnumerator<Instance> it = VirtualMachines.GetEnumerator();
                it.MoveNext();

                List<string> hosts = new List<string>();
                List<string> peers = new List<string>();
                List<string> workpeers = new List<string>();
                List<string> address = new List<string>();
                List<string> workers = new List<string>();
                List<string> workers_host_file = new List<string>();
                int count = 0;
                // int portWorkers = 4000;

                string private_address_root = it.Current.NetworkInterfaces[0].NetworkIP;
                //string public_address_root = it.Current.NetworkInterfaces[0].AccessConfigs[0].NatIP;

                peers.Add("root");
                hosts.Add("127.0.0.1 localhost");
                hosts.Add(private_address_root + " root");
                address.Add(private_address_root);
                workers.Add("-n 1 " + Path.Combine(MONO_HOME, "bin/mono-service") + " -l:Worker.lock " + Path.Combine(HPCShelf_HOME, "BackendServices", "bin", "WorkerService.exe") + " --port 5000 --debug --no-deamon");
                workers_host_file.Add(private_address_root + " 5000");

                while (it.MoveNext())
                {
                    Instance vm = it.Current;

                    string private_address = vm.NetworkInterfaces[0].NetworkIP;
                   // string public_address = vm.NetworkInterfaces[0].AccessConfigs[0].NatIP;
                    peers.Add("peer" + (count));
                    workpeers.Add("peer" + (count));
                    hosts.Add(private_address + " peer" + count);
                    address.Add(private_address);
                    workers.Add("-n 1 " + Path.Combine(MONO_HOME, "bin/mono-service") + " -l:Worker" + count + ".lock " + Path.Combine(HPCShelf_HOME, "BackendServices", "bin", "WorkerService.exe") + " --port 5000 --debug --no-deamon");
                    workers_host_file.Add(private_address + " 5000");
                    count++;
                }

                Utils.fileWrite(Utils.SCRIPTS, string.Format("peers.{0}", deploy_id), peers.ToArray());
                Utils.fileWrite(Utils.SCRIPTS, string.Format("workpeers.{0}", deploy_id), workpeers.ToArray());
                Utils.fileWrite(Utils.SCRIPTS, string.Format("hosts.{0}", deploy_id), hosts.ToArray());
                Utils.fileWrite(Utils.SCRIPTS, string.Format("address.{0}", deploy_id), address.ToArray());
                Utils.fileWrite(Utils.SCRIPTS, string.Format("workers.{0}", deploy_id), workers.ToArray());
                Utils.fileWrite(Utils.SCRIPTS, string.Format("workers_host_file.{0}", deploy_id), workers_host_file.ToArray());

                Console.WriteLine("generatePeerHosts exit: {0}", deploy_id);
            }
        }
    }
}
